lazy val akkaHttpVersion = "10.1.7"
lazy val akkaVersion = "2.5.19"
lazy val scalaTestVersion = "3.0.1"
lazy val scalaCheckVersion = "1.14.0"
lazy val catsVersion = "1.5.0"
lazy val scalaTime = "2.20.0"

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization    := "com.newmotion",
      scalaVersion    := "2.12.8"
    )),
    name := "hermes",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-stream" % akkaVersion,
      "org.typelevel" %% "cats-core" % catsVersion,
      "com.github.nscala-time" %% "nscala-time" % scalaTime,

      "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion % Test,
      "com.typesafe.akka" %% "akka-testkit" % akkaVersion % Test,
      "org.scalatest" %% "scalatest" % scalaTestVersion % Test,
      "org.scalacheck" %% "scalacheck" % scalaCheckVersion % Test
    )
  )
