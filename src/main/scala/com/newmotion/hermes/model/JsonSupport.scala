package com.newmotion.hermes.model

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import com.newmotion.hermes.BillingActor.{ BillingError, TariffCreated }
import org.joda.time.DateTime
import org.joda.time.format.{ DateTimeFormatter, DateTimeFormat => JodaFormat }
import spray.json.{ DefaultJsonProtocol, JsString, JsValue, RootJsonFormat, _ }

trait JsonSupport extends SprayJsonSupport {

  implicit object DateTimeFormat extends RootJsonFormat[DateTime] {

    val formatter: DateTimeFormatter = JodaFormat.forPattern("yyyy-MM-dd'T'HH:mm:ssZZ")

    def write(obj: DateTime): JsValue = {
      JsString(formatter.print(obj))
    }

    def read(json: JsValue): DateTime = json match {
      case JsString(s) => try {
        formatter.parseDateTime(s)
      } catch {
        case _: Throwable => error(s)
      }
      case _ =>
        error(json.toString())
    }

    def error(v: Any): DateTime = {
      val example = formatter.print(0)
      deserializationError(f"'$v' is not a valid date value. Dates must be in compact ISO-8601 format, e.g. '$example'")
    }
  }

  import DefaultJsonProtocol._

  implicit val tariffJsonFormat = jsonFormat4(Tariff.apply)
  implicit val validationErrorListFormat = jsonFormat1(ValidationErrorList)
  implicit val tariffCreatedFormat = jsonFormat1(TariffCreated)
  implicit val billingErrorFormat = jsonFormat1(BillingError)
  implicit val chargeSessionFormat = jsonFormat3(ChargeSession.apply)
  implicit val billedSessionFormat = jsonFormat2(BilledSession.apply)
}
