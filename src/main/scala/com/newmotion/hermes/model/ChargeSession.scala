package com.newmotion.hermes.model

import cats.data.Validated
import cats.data.Validated.{ invalid, valid }
import com.github.nscala_time.time.Imports._
import org.joda.time.DateTime
import ChargeSession._

case class ChargeSession(userId: Int, start: DateTime, end: DateTime) {

  def validate(): Validated[List[String], ChargeSession] = {
    ChargeSession.runValidations(
      this,
      validateStart,
      validateEnd,
      validateDuration)
  }

  def overlaps(other: ChargeSession): Boolean = {
    val thisInterval = new Interval(this.start, this.end)
    val otherInterval = new Interval(other.start, other.end)
    thisInterval.overlaps(otherInterval)
  }

}

case class BilledSession(chargeSession: ChargeSession, tariff: Tariff)

case object ChargeSession extends Validator {
  val START_ERROR_MSG = "Start time should be in the past"
  val END_ERROR_MSG = "End time should be in the past"
  val DURATION_ERROR_MSG = "End time should be after start time, and the duration should be > 0"

  def validateStart(chargeSession: ChargeSession): Validated[String, ChargeSession] =
    if (DateTime.now > chargeSession.start) valid(chargeSession)
    else invalid(START_ERROR_MSG)

  def validateEnd(chargeSession: ChargeSession): Validated[String, ChargeSession] =
    if (DateTime.now > chargeSession.end) valid(chargeSession)
    else invalid(END_ERROR_MSG)

  def validateDuration(chargeSession: ChargeSession): Validated[String, ChargeSession] =
    if (chargeSession.start < chargeSession.end) valid(chargeSession)
    else invalid(DURATION_ERROR_MSG)
}
