package com.newmotion.hermes.model

sealed trait DomainError {
  def message(): String
}

final case class ValidationErrorList(errors: Seq[String]) extends DomainError {
  override def message(): String = errors.mkString(",")
}

case object DateAlreadyExists extends DomainError {
  val message: String = "A tariff already exists for the specific date and time"
}

case object SessionAlreadyExists extends DomainError {
  val message: String = "A session already exists for this duration"
}