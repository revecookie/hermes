package com.newmotion.hermes.model

import cats.data.Validated
import cats.data.Validated.{ invalid, valid }
import com.newmotion.hermes.model.Tariff._
import org.joda.time.DateTime
import com.github.nscala_time.time.Imports._

case class Tariff(
  baseFeePerkWh: BigDecimal,
  parkingFeePerHour: Option[BigDecimal],
  serviceFeePercentage: Double,
  startsAt: DateTime) {

  def validate(): Validated[List[String], Tariff] = {
    Tariff.runValidations(
      this,
      validateBaseFeePerkWh,
      validateServiceFeePercentage,
      validateParkingFeePerHour,
      validateStartsAt)
  }

}

object Tariff extends Validator {

  val SERVICE_FEE_ERROR_MSG = "Service fee percentage must be between 0.0 (exclusive) and 0.5"
  val BASE_FEE_ERROR_MSG = "Base fee must be >= 0"
  val PARKING_FEE_ERROR_MSG = "Base fee must be >= 0"
  val STARTS_AT_ERROR_MSG = "Starting date should be in the future"

  def validateServiceFeePercentage(tariff: Tariff): Validated[String, Tariff] =
    if (tariff.serviceFeePercentage > 0.0 && tariff.serviceFeePercentage <= 0.5) valid(tariff)
    else invalid(SERVICE_FEE_ERROR_MSG)

  def validateBaseFeePerkWh(tariff: Tariff): Validated[String, Tariff] =
    if (tariff.baseFeePerkWh >= BigDecimal.apply(0)) valid(tariff)
    else invalid(BASE_FEE_ERROR_MSG)

  def validateParkingFeePerHour(tariff: Tariff): Validated[String, Tariff] =
    tariff.parkingFeePerHour match {
      case None => valid(tariff)
      case Some(pf) => if (pf >= BigDecimal.apply(0)) valid(tariff) else invalid(PARKING_FEE_ERROR_MSG)
    }

  def validateStartsAt(tariff: Tariff): Validated[String, Tariff] =
    if (DateTime.now <= tariff.startsAt) valid(tariff) else invalid(STARTS_AT_ERROR_MSG)
}
