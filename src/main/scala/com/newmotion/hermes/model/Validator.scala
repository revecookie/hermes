package com.newmotion.hermes.model

import cats.data.Validated
import cats.data.Validated.{ Invalid, invalid, valid }

trait Validator {
  def runValidations[S, T](obj: T, validations: (T => Validated[S, T])*): Validated[List[S], T] =
    combine(obj, validations.toList.map(_.apply(obj)))

  def combine[S, T](obj: T, validations: List[Validated[S, T]]): Validated[List[S], T] = {
    val errors = validations.collect { case Invalid(error) => error }
    if (errors.isEmpty) valid(obj) else invalid(errors)
  }
}
