package com.newmotion.hermes.model

import com.github.nscala_time.time.Imports.Interval
import org.joda.time.DateTime

import scala.math.BigDecimal.RoundingMode

case class UserSession(
  start: DateTime,
  end: DateTime,
  baseFeePerkWh: BigDecimal,
  parkingFeePerHour: BigDecimal,
  serviceFeePercentage: Double,
  startsAt: DateTime,
  price: BigDecimal,
  serviceFee: BigDecimal)

object UserSession {

  def from(session: BilledSession): UserSession = {

    val charge = session.chargeSession
    val tariff = session.tariff
    val parkingFee = tariff.parkingFeePerHour.getOrElse(BigDecimal.valueOf(0))

    val duration = new Interval(charge.start, charge.end).toDuration.getStandardSeconds
    val (price, servicePrice) = calculatePrices(tariff, duration)

    UserSession(
      charge.start,
      charge.end,
      tariff.baseFeePerkWh,
      parkingFee,
      tariff.serviceFeePercentage,
      tariff.startsAt,
      price,
      servicePrice)
  }

  def calculatePrices(tariff: Tariff, duration: Long): (BigDecimal, BigDecimal) = {

    val pricePerSec = tariff.baseFeePerkWh / (60 * 60)
    val parkingPerSec = tariff.parkingFeePerHour.getOrElse(BigDecimal.valueOf(0)) / (60 * 60)
    val price = pricePerSec * duration + parkingPerSec * duration
    val servicePrice = price * tariff.serviceFeePercentage

    (price.setScale(2, RoundingMode.HALF_UP), servicePrice.rounded.setScale(2, RoundingMode.HALF_UP))
  }
}
