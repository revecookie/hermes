package com.newmotion.hermes

import akka.actor.{ ActorRef, ActorSystem, Props }
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import akka.http.scaladsl.server.RouteConcatenation._

object HermesServer extends App with BillingRoutes {

  implicit val system: ActorSystem = ActorSystem("hermesServer")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val store: Store = InMemoryStore

  val PORT = 8080
  val DOMAIN = "localhost"

  val billingActor: ActorRef = system.actorOf(Props(new BillingActor), "billingActor")

  lazy val routes: Route = companyRoutes ~ userRoutes

  InMemoryStore.initializeWithSampleData()

  Http().bindAndHandle(routes, DOMAIN, PORT)

  println(s"Hermes server online at http://$DOMAIN:$PORT/")

  Await.result(system.whenTerminated, Duration.Inf)

}
