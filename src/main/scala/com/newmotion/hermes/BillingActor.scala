package com.newmotion.hermes

import akka.actor.{ Actor, ActorLogging }
import com.newmotion.hermes.BillingActor._
import com.newmotion.hermes.model.{ BilledSession, ChargeSession, Tariff, UserSession }
import com.github.nscala_time.time.Imports._

object BillingActor {

  val NO_TARIFFS_AVAILABLE = "The session could not be billed. There were not tariffs available for the duration of the session."
  val SESSION_OVERLAP = "There is an existing session that overlaps with this one."

  trait BillingActorRequest

  final case class CreateTariff(tariff: Tariff) extends BillingActorRequest

  final case class BillSession(session: ChargeSession) extends BillingActorRequest

  final case class GetUserSessions(userId: Int) extends BillingActorRequest

  trait BillingActorResponse

  final case class TariffCreated(message: String = "New tariff created successfully") extends BillingActorResponse

  final case class SessionBilled(session: BilledSession) extends BillingActorResponse

  final case class BilledSessions(sessions: List[BilledSession])

  final case class UserSessions(sessions: List[UserSession]) extends BillingActorResponse

  final case class BillingError(message: String) extends BillingActorResponse

}

class BillingActor()(implicit val store: Store) extends Actor with ActorLogging {

  def receive: Receive = {
    case CreateTariff(tariff) => sender() ! createTariff(tariff)
    case BillSession(session) => sender() ! billSession(session)
    case GetUserSessions(id) => sender() ! UserSessions(store.getBilledSessions(id).map(UserSession.from))
  }

  def createTariff(tariff: Tariff): BillingActorResponse = {
    store.saveTariff(tariff) match {
      case Right(_) => TariffCreated()
      case Left(error) => BillingError(error.message())
    }
  }

  def billSession(session: ChargeSession): BillingActorResponse = {

    val availableTariffs = store.getTariffs.filter(_.startsAt <= session.start).sortBy(_.startsAt)

    if (availableTariffs.isEmpty) BillingError(NO_TARIFFS_AVAILABLE)
    else if (sessionsOverlap(session)) BillingError(SESSION_OVERLAP)
    else storeSession(availableTariffs.maxBy(_.startsAt), session)
  }

  def sessionsOverlap(session: ChargeSession): Boolean = {
    val userSessions = store.getBilledSessions(session.userId)
    userSessions.exists(_.chargeSession.overlaps(session))
  }

  def storeSession(applicableTariff: Tariff, session: ChargeSession): BillingActorResponse = {
    val billedSession = BilledSession(session, applicableTariff)
    store.saveBilledSession(billedSession) match {
      case Right(savedSession) => SessionBilled(savedSession)
      case Left(error) => BillingError(error.message())
    }
  }
}