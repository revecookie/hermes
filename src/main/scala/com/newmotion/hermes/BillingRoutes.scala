package com.newmotion.hermes

import akka.actor.{ ActorRef, ActorSystem }
import akka.event.Logging
import akka.http.scaladsl.common.{ CsvEntityStreamingSupport, EntityStreamingSupport }
import akka.http.scaladsl.marshalling.{ Marshaller, Marshalling }
import akka.http.scaladsl.model.{ ContentTypes, HttpResponse, StatusCodes }
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.directives.Credentials
import akka.http.scaladsl.server.directives.MethodDirectives.post
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import akka.http.scaladsl.server.{ ExceptionHandler, Route }
import akka.pattern.ask
import akka.stream.scaladsl.Source
import akka.util.{ ByteString, Timeout }
import cats.data.Validated.{ Invalid, Valid }
import com.newmotion.hermes.BillingActor._
import com.newmotion.hermes.model._

import scala.concurrent.Future
import scala.concurrent.duration._

trait BillingRoutes extends JsonSupport {

  implicit def system: ActorSystem

  lazy val log = Logging(system, classOf[BillingRoutes])

  implicit def myExceptionHandler: ExceptionHandler =
    ExceptionHandler {
      case _ =>
        extractUri { uri =>
          log.error(s"Unexpected error for uri: $uri")
          complete(HttpResponse(StatusCodes.InternalServerError, entity = "Unexpected Error"))
        }
    }

  implicit val sessionAsCsv: Marshaller[UserSession, ByteString] = Marshaller.strict[UserSession, ByteString] { s =>
    Marshalling.WithFixedContentType(ContentTypes.`text/csv(UTF-8)`, () => {
      ByteString(List(s.start, s.end, s.baseFeePerkWh, s.parkingFeePerHour, s.serviceFeePercentage, s.startsAt, s.price, s.serviceFee).mkString(","))
    })
  }

  implicit val csvStreaming: CsvEntityStreamingSupport = EntityStreamingSupport.csv()

  def billingActor: ActorRef

  implicit lazy val timeout: Timeout = Timeout(5.seconds)

  def myUserPassAuthenticator(credentials: Credentials): Option[String] =
    credentials match {
      case p @ Credentials.Provided(id) if p.verify("1234") => Some(id)
      case _ => None
    }

  lazy val companyRoutes: Route =

    pathPrefix("billing") {
      path("tariff") {
        Route.seal {
          post {
            authenticateBasic(realm = "secure site", myUserPassAuthenticator) { userName =>
              log.debug(s"User: $userName posted tariff")

              entity(as[Tariff]) { maybeTariff =>
                maybeTariff.validate() match {
                  case Valid(tariff) =>
                    val maybeCreated: Future[BillingActorResponse] =
                      (billingActor ? CreateTariff(tariff)).mapTo[BillingActorResponse]

                    onSuccess(maybeCreated) {
                      case _: TariffCreated => complete(TariffCreated())
                      case error: BillingError => complete(StatusCodes.Conflict, error)
                      case _ => complete(StatusCodes.InternalServerError)
                    }

                  case Invalid(failures) => complete(StatusCodes.BadRequest, ValidationErrorList(failures))
                }

              }
            }
          }
        }
      } ~
        path("session") {
          post {
            entity(as[ChargeSession]) { maybeSession =>
              maybeSession.validate() match {
                case Valid(session) =>
                  val maybeBillingSession: Future[BillingActorResponse] =
                    (billingActor ? BillSession(session)).mapTo[BillingActorResponse]

                  onSuccess(maybeBillingSession) {
                    case SessionBilled(billedSession) => complete(billedSession)
                    case error: BillingError => complete(StatusCodes.BadRequest, error)
                    case _ => complete(StatusCodes.InternalServerError)
                  }

                case Invalid(failures) => complete(StatusCodes.BadRequest, ValidationErrorList(failures))
              }

            }
          }
        }
    }

  lazy val userRoutes: Route =
    pathPrefix("user") {
      path(IntNumber / "sessions") { id =>
        get {
          val maybeSessions: Future[BillingActorResponse] =
            (billingActor ? GetUserSessions(id)).mapTo[BillingActorResponse]

          onSuccess(maybeSessions) {
            case s: UserSessions => complete(Source(s.sessions))
            case error: BillingError => complete(StatusCodes.NotFound, error)
            case _ => complete(StatusCodes.InternalServerError)
          }
        }
      }
    }
}
