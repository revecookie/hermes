package com.newmotion.hermes

import com.newmotion.hermes.model._
import org.joda.time.DateTime

trait Store {
  def getTariffs: Seq[Tariff]
  def saveTariff(tariff: Tariff): Either[DomainError, Tariff]
  def saveBilledSession(billedSession: BilledSession): Either[DomainError, BilledSession]
  def getBilledSessions(user: Int): List[BilledSession]
}

object InMemoryStore extends Store {

  var tariffs: List[Tariff] = Nil
  var billedSessions: Map[Int, List[BilledSession]] = Map()

  def initializeWithSampleData(): Unit = {
    val tariffDate = DateTime.parse("1991-12-08T08:00:00")
    val tariff = Tariff(BigDecimal.valueOf(600), None, 0.1, tariffDate)
    tariffs = List(tariff)
    val sessionADate = DateTime.parse("1993-02-15T04:00:00")
    val sessionA = BilledSession(ChargeSession(42, sessionADate, sessionADate.plusHours(2)), tariff)

    val sessionBDate = DateTime.parse("1996-07-22T10:00:00")
    val sessionB = BilledSession(ChargeSession(42, sessionBDate, sessionBDate.plusHours(1)), tariff)

    billedSessions = Map(42 -> List(sessionA, sessionB))
  }

  override def getTariffs: Seq[Tariff] = tariffs

  override def saveTariff(tariff: Tariff): Either[DomainError, Tariff] = {
    if (tariffs.map(_.startsAt).contains(tariff.startsAt)) Left(DateAlreadyExists)
    else {
      tariffs = tariffs :+ tariff
      Right(tariff)
    }
  }

  override def saveBilledSession(billedSession: BilledSession): Either[DomainError, BilledSession] = {
    val user = billedSession.chargeSession.userId
    val userSessions = billedSessions.getOrElse(user, Nil)
    val enhancedSessions = userSessions :+ billedSession
    billedSessions = billedSessions + (user -> enhancedSessions)
    Right(billedSession)
  }

  override def getBilledSessions(userId: Int): List[BilledSession] = {
    billedSessions.getOrElse(userId, Nil)
  }

}
