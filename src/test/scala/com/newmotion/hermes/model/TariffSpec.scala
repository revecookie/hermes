package com.newmotion.hermes.model

import cats.data.Validated
import org.scalacheck.Gen
import org.scalatest.{ Matchers, PropSpec }
import org.scalatest.prop.PropertyChecks
import com.newmotion.hermes.model.Tariff._
import com.github.nscala_time.time.Imports._

class TariffSpec extends PropSpec with PropertyChecks with Matchers {

  val moneyGenerator = for {
    baseFeeGen <- Gen.chooseNum(0, 100000000)
    baseFeePerkWh = BigDecimal.valueOf(baseFeeGen)
    parkingGen <- Gen.chooseNum(0, 100000000)
    parkingFeePerHour = Gen.option(BigDecimal.valueOf(parkingGen))
  } yield (baseFeePerkWh, parkingFeePerHour.sample.get)

  property("Should successfully create a Tariff") {
    forAll(moneyGenerator, minSuccessful(100)) {
      case (baseFeePerkWh, parkingFeePerHour) =>
        val lastDayOfFrameProduction: Validated[List[String], Tariff] =
          Tariff(baseFeePerkWh, parkingFeePerHour, 0.1, DateTime.now + 1.day).validate()
        lastDayOfFrameProduction.isValid shouldBe true
    }

  }

  property("Should fail to create tariff if service fee is invalid") {
    forAll(moneyGenerator, minSuccessful(100)) {
      case (baseFeePerkWh, parkingFeePerHour) =>
        val lastDayOfFrameProduction: Validated[List[String], Tariff] =
          Tariff(baseFeePerkWh, parkingFeePerHour, 0.0, DateTime.now + 1.day).validate()
        lastDayOfFrameProduction.isValid shouldBe false
        lastDayOfFrameProduction.leftMap(errors => {
          errors.size shouldBe 1
          errors.contains(SERVICE_FEE_ERROR_MSG) shouldBe true
        })
    }
  }

  property("Should fail to create tariff if base fee is invalid") {
    forAll(moneyGenerator, minSuccessful(100)) {
      case (_, parkingFeePerHour) =>
        val lastDayOfFrameProduction: Validated[List[String], Tariff] =
          Tariff(BigDecimal.apply(-2), parkingFeePerHour, 0.2, DateTime.now + 1.day).validate()
        lastDayOfFrameProduction.isValid shouldBe false
        lastDayOfFrameProduction.leftMap(errors => {
          errors.size shouldBe 1
          errors.contains(BASE_FEE_ERROR_MSG) shouldBe true
        })
    }
  }

  property("Should fail to create tariff if parking fee is invalid") {
    forAll(moneyGenerator, minSuccessful(100)) {
      case (baseFeePerkWh, _) =>
        val lastDayOfFrameProduction: Validated[List[String], Tariff] =
          Tariff(baseFeePerkWh, Some(BigDecimal.apply(-2)), 0.2, DateTime.now + 1.day).validate()
        lastDayOfFrameProduction.isValid shouldBe false
        lastDayOfFrameProduction.leftMap(errors => {
          errors.size shouldBe 1
          errors.contains(PARKING_FEE_ERROR_MSG) shouldBe true
        })
    }
  }

  property("Should fail to create tariff if all params are invalid") {
    val lastDayOfFrameProduction: Validated[List[String], Tariff] =
      Tariff(BigDecimal.apply(-1), Some(BigDecimal.apply(-2)), 0.0, DateTime.now - 1.day).validate()
    lastDayOfFrameProduction.isValid shouldBe false
    lastDayOfFrameProduction.leftMap(errors => {
      errors.size shouldBe 4
      errors.contains(PARKING_FEE_ERROR_MSG) shouldBe true
      errors.contains(BASE_FEE_ERROR_MSG) shouldBe true
      errors.contains(SERVICE_FEE_ERROR_MSG) shouldBe true
      errors.contains(STARTS_AT_ERROR_MSG) shouldBe true
    })
  }

}
