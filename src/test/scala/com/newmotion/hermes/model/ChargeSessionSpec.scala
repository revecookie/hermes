package com.newmotion.hermes.model

import org.scalatest.{ FunSpec, Matchers }
import com.github.nscala_time.time.Imports._
import ChargeSession._

class ChargeSessionSpec extends FunSpec with Matchers {

  it("Should create charge session successfully") {

    val correct = ChargeSession(1, DateTime.now - 2.hours, DateTime.now - 1.hour).validate()

    correct.isValid shouldBe true
  }

  it("Should return error if start date is wrong") {

    val wrong = ChargeSession(1, DateTime.now + 1.day, DateTime.now - 1.day).validate()

    wrong.isValid shouldBe false
    wrong.leftMap(errors => {
      errors.size shouldBe 2
      errors.contains(START_ERROR_MSG) shouldBe true
      errors.contains(DURATION_ERROR_MSG) shouldBe true
    })
  }

  it("Should return error if end date is wrong") {

    val wrong = ChargeSession(1, DateTime.now - 1.day, DateTime.now + 1.day).validate()

    wrong.isValid shouldBe false
    wrong.leftMap(errors => {
      errors.size shouldBe 1
      errors.contains(END_ERROR_MSG) shouldBe true
    })
  }

  it("should return true if dates overlap") {
    val sessionA = ChargeSession(1, DateTime.now - 2.days, DateTime.now - 1.day)
    val sessionB = ChargeSession(1, DateTime.now - 2.days, DateTime.now - 1.day)

    sessionA.overlaps(sessionB) shouldBe true

    val sessionC = ChargeSession(1, DateTime.now - 1.days - 1.hour, DateTime.now - 10.hours)
    sessionC.overlaps(sessionB) shouldBe true

  }
  it("should return false if dates do not overlap") {
    val sessionA = ChargeSession(1, DateTime.now - 2.days, DateTime.now - 1.day)
    val sessionB = ChargeSession(1, DateTime.now - 1.days, DateTime.now - 1.hour)

    sessionA.overlaps(sessionB) shouldBe false

    val sessionC = ChargeSession(1, DateTime.now - 2.minutes, DateTime.now - 1.minute)
    sessionC.overlaps(sessionB) shouldBe false

  }

}
