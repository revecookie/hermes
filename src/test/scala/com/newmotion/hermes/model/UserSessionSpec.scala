package com.newmotion.hermes.model

import org.scalatest.{ FunSpec, Matchers }
import org.joda.time.DateTime

class UserSessionSpec extends FunSpec with Matchers {

  private val ONE_HOUR = 60 * 60
  private val HALF_HOUR = 30 * 60
  private val TEN_SECONDS = 10

  it("should calculate prices correctly for hour sessions") {
    val tariff = Tariff(BigDecimal.valueOf(5), Some(BigDecimal.valueOf(2)), 0.5, DateTime.now)

    val (totalA, serviceFeeA) = UserSession.calculatePrices(tariff, ONE_HOUR)
    totalA shouldBe BigDecimal.valueOf(7)
    serviceFeeA shouldBe BigDecimal.valueOf(7 * 0.5)

    val (totalB, serviceFeeB) = UserSession.calculatePrices(tariff, HALF_HOUR)
    totalB shouldBe BigDecimal.valueOf(3.5)
    serviceFeeB shouldBe BigDecimal.valueOf(3.5 * 0.5)

  }

  it("should calculate prices correctly for seconds") {
    val bigAmount = Tariff(BigDecimal.valueOf(6000), Some(BigDecimal.valueOf(2000)), 0.5, DateTime.now)
    val (totalC, serviceFeeC) = UserSession.calculatePrices(bigAmount, TEN_SECONDS)
    totalC shouldBe BigDecimal.valueOf(22.22)
    serviceFeeC shouldBe BigDecimal.valueOf(11.11)

    val smallAmount = Tariff(BigDecimal.valueOf(1), Some(BigDecimal.valueOf(2)), 0.5, DateTime.now)
    val (totalD, serviceFeeD) = UserSession.calculatePrices(smallAmount, TEN_SECONDS)
    totalD shouldBe BigDecimal.valueOf(0.01)
    serviceFeeD shouldBe BigDecimal.valueOf(0.00)
  }

}
