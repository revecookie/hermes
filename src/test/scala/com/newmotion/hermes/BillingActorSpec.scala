package com.newmotion.hermes

import akka.actor.{ ActorRef, ActorSystem, Props }
import akka.pattern.ask
import akka.testkit.TestKit
import akka.util.Timeout
import BillingActor._
import com.newmotion.hermes.model.{ BilledSession, ChargeSession, Tariff, UserSession }
import org.joda.time.DateTime
import org.scalatest._

import scala.concurrent.Future
import scala.concurrent.duration._

class BillingActorSpec(_system: ActorSystem) extends TestKit(_system)
  with Matchers
  with AsyncFlatSpecLike
  with BeforeAndAfterAll {

  def this() = this(ActorSystem("BillingActorSpec"))

  implicit lazy val timeout: Timeout = Timeout(5.seconds)
  implicit val store: Store = InMemoryStore
  val billingActor: ActorRef = system.actorOf(Props(new BillingActor), "billingActor")
  val nowDate: DateTime = DateTime.now

  override def afterAll: Unit = {
    shutdown(system)
  }

  it should "Return message when tariff is successfully created" in {
    val response = billingActor ? CreateTariff(Tariff(BigDecimal(0), None, 0.2, DateTime.now.plusDays(1)))
    response.map(r => r shouldBe TariffCreated())
  }

  it should "Return billed session" in {
    createSessionSuccessfully

  }

  it should "Return error if there is no tariff for the session" in {
    val tariff = Tariff(BigDecimal.valueOf(1), None, 0.2, nowDate.plusDays(2))
    InMemoryStore.tariffs = List(tariff)
    InMemoryStore.billedSessions = Map()
    val chargeSession = ChargeSession(1, nowDate.minusHours(2), nowDate.minusHours(1))
    val response = billingActor ? BillSession(chargeSession)

    response.map(error => {
      error shouldBe BillingError(NO_TARIFFS_AVAILABLE)
    })

  }

  it should "Return error if session overlaps with other session" in {
    val tariff = Tariff(BigDecimal.valueOf(1), None, 0.2, nowDate.plusDays(2))
    InMemoryStore.tariffs = List(tariff)
    InMemoryStore.billedSessions = Map()

    createSessionSuccessfully

    val chargeSession = ChargeSession(1, nowDate.minusHours(1).minusMinutes(30), nowDate.minusMinutes(30))
    val response = billingActor ? BillSession(chargeSession)

    response.map(error => {
      error shouldBe BillingError(SESSION_OVERLAP)
    })

  }

  it should "Return the sessions of a user successfully" in {
    val chargeSession = ChargeSession(1, nowDate.minusHours(2), nowDate.minusHours(1))
    val tariff = Tariff(BigDecimal.valueOf(1), None, 0.2, nowDate.plusDays(2))
    val sessionA = BilledSession(chargeSession, tariff)
    InMemoryStore.billedSessions = Map(1 -> List(sessionA))

    val response = billingActor ? GetUserSessions(1)

    response.map(result => {
      result shouldBe UserSessions(List(
        UserSession(
          nowDate.minusHours(2),
          nowDate.minusHours(1),
          1,
          0,
          0.2,
          nowDate.plusDays(2),
          1.000000000000000000000000000000000,
          0.2000000000000000000000000000000000)))
    })

  }

  it should "Return the sessions of a user successfully when user is not defined" in {
    InMemoryStore.billedSessions = Map()

    val response = billingActor ? GetUserSessions(1)

    response.map(result => {
      result shouldBe UserSessions(List())
    })

  }

  private def createSessionSuccessfully: Future[Assertion] = {
    val tariff = Tariff(BigDecimal.valueOf(1), None, 0.2, nowDate.minusDays(1))
    val tariffOld = Tariff(BigDecimal.valueOf(1), None, 0.2, nowDate.minusDays(2))
    val tariffOlder = Tariff(BigDecimal.valueOf(1), None, 0.2, nowDate.minusYears(1))
    InMemoryStore.tariffs = List(tariff, tariffOld, tariffOlder)
    InMemoryStore.billedSessions = Map()
    val chargeSession = ChargeSession(1, nowDate.minusHours(2), nowDate.minusHours(1))
    val response = billingActor ? BillSession(chargeSession)

    response.map(billedSession => {
      billedSession shouldBe SessionBilled(BilledSession(chargeSession, tariff))
    })
  }

}
