package com.newmotion.hermes

import akka.actor.{ ActorRef, Props }
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model.headers.{ BasicHttpCredentials, HttpChallenge, `WWW-Authenticate` }
import akka.http.scaladsl.model.{ MessageEntity, StatusCodes }
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.newmotion.hermes.model.{ BilledSession, ChargeSession, Tariff }
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{ Assertion, FunSpec, Matchers }
import spray.json._
import org.joda.time.DateTime
import com.github.nscala_time.time.Imports._
import akka.http.scaladsl.server.RouteConcatenation._

class BillingRoutesSpec extends FunSpec
  with Matchers
  with ScalaFutures
  with ScalatestRouteTest
  with BillingRoutes {

  implicit val store: InMemoryStore.type = InMemoryStore

  override val billingActor: ActorRef =
    system.actorOf(Props(new BillingActor), "billingActor")

  lazy val routes: Route = companyRoutes ~ userRoutes
  val now: DateTime = DateTime.now
  val validCredentials = BasicHttpCredentials("Admin", "1234")

  def successfullTariffCreation(date: DateTime): Assertion = {
    val expected = """{"message": "New tariff created successfully"}"""
    val tariff = Tariff(BigDecimal(1), Some(BigDecimal(1)), 0.1, date)
    val tariffEntity = Marshal(tariff).to[MessageEntity].futureValue

    val request = Post("/billing/tariff").withEntity(tariffEntity)

    request ~> addCredentials(validCredentials) ~> routes ~> check {
      status shouldBe StatusCodes.OK
      entityAs[String].parseJson shouldBe expected.parseJson
    }
  }

  it("should return 401 if user is unauthorized") {
    val tariff = Tariff(BigDecimal(1), Some(BigDecimal(1)), 0.0, now + 1.day)
    val tariffEntity = Marshal(tariff).to[MessageEntity].futureValue
    val request = Post("/billing/tariff").withEntity(tariffEntity)

    request ~> routes ~> check {
      status shouldBe StatusCodes.Unauthorized
      responseAs[String] shouldEqual "The resource requires authentication, which was not supplied with the request"
      header[`WWW-Authenticate`].get.challenges.head shouldBe HttpChallenge("Basic", Some("secure site"), Map("charset" → "UTF-8"))

    }
  }

  it("should successfully create a new tariff (POST /billing/tariff)") {
    successfullTariffCreation(now + 1.day)
  }

  it("should fail to create a new tariff when service fee is incorrect (POST /billing/tariff)") {
    val expected = """{"errors":["Service fee percentage must be between 0.0 (exclusive) and 0.5"]}"""
    val tariff = Tariff(BigDecimal(1), Some(BigDecimal(1)), 0.0, now + 1.day)
    val tariffEntity = Marshal(tariff).to[MessageEntity].futureValue

    val request = Post("/billing/tariff").withEntity(tariffEntity)

    request ~> addCredentials(validCredentials) ~> routes ~> check {
      status shouldBe StatusCodes.BadRequest
      entityAs[String].parseJson shouldBe expected.parseJson
    }
  }

  it("should fail to create a new tariff when date is incorrect (POST /billing/tariff)") {
    val expected = """{"errors":["Starting date should be in the future"]}"""
    val tariff = Tariff(BigDecimal(1), Some(BigDecimal(1)), 0.2, now - 1.day)
    val tariffEntity = Marshal(tariff).to[MessageEntity].futureValue

    val request = Post("/billing/tariff").withEntity(tariffEntity)

    request ~> addCredentials(validCredentials) ~> routes ~> check {
      status shouldBe StatusCodes.BadRequest
      entityAs[String].parseJson shouldBe expected.parseJson
    }
  }

  it("should fail to create a new tariff when date already exists (POST /billing/tariff)") {

    val collisionDate = now + 2.day
    successfullTariffCreation(collisionDate)

    val expected = """{"message":"A tariff already exists for the specific date and time"}"""
    val tariff = Tariff(BigDecimal(3), Some(BigDecimal(4)), 0.3, collisionDate)
    val tariffEntity = Marshal(tariff).to[MessageEntity].futureValue

    val request = Post("/billing/tariff").withEntity(tariffEntity)

    request ~> addCredentials(validCredentials) ~> routes ~> check {
      status shouldBe StatusCodes.Conflict
      entityAs[String].parseJson shouldBe expected.parseJson
    }
  }

  it("should successfully create a new Billing session (POST /billing/session)") {

    val tariff = Tariff(BigDecimal.valueOf(1), None, 0.2, now - 2.days)
    InMemoryStore.tariffs = List(tariff)

    val chargeSession = ChargeSession(2, now - 2.hours, now - 1.hour)
    val chargeSessionEntity = Marshal(chargeSession).to[MessageEntity].futureValue

    val request = Post("/billing/session").withEntity(chargeSessionEntity)

    request ~> routes ~> check {
      status shouldBe StatusCodes.OK
      val result = entityAs[BilledSession]
      result.chargeSession.userId shouldBe 2
      result.tariff.startsAt.secondOfDay() shouldBe tariff.startsAt.secondOfDay()

    }
  }

  it("should successfully return the sessions of the user (GET user/userId/sessions)") {

    val tariff = Tariff(BigDecimal.valueOf(1), None, 0.2, now - 2.days)
    InMemoryStore.tariffs = List(tariff)
    InMemoryStore.billedSessions = Map()

    val chargeSession = ChargeSession(2, now - 4.hours, now - 3.hour)
    val chargeSessionEntity = Marshal(chargeSession).to[MessageEntity].futureValue

    val chargeSessionB = ChargeSession(2, now - 2.hours, now - 1.hour)
    val chargeSessionEntityB = Marshal(chargeSessionB).to[MessageEntity].futureValue

    val request = Post("/billing/session").withEntity(chargeSessionEntity)
    val requestB = Post("/billing/session").withEntity(chargeSessionEntityB)

    request ~> routes ~> check {
      status shouldBe StatusCodes.OK
    }

    requestB ~> routes ~> check {
      status shouldBe StatusCodes.OK
    }

    val requestSessions = Get("/user/2/sessions")

    requestSessions ~> routes ~> check {
      status shouldBe StatusCodes.OK
      val result = entityAs[String].split("\n").toList
      result.size shouldBe 2
    }
  }

}
