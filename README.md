# HERMES

The name of the project is inspired by [Hermes Conrad](https://www.wikiwand.com/en/Hermes_Conrad) a
Bureaucrat Grade 35 and accountant of the Planet Express delivery company from the Futurama series.

![Hermes](https://upload.wikimedia.org/wikipedia/en/a/aa/Hermes_Conrad_promo_pic.png "Hermes")

## Introduction
This is a service aimed to manage service fees on charging sessions. There are 3 types of users:
1. The company (in this case TVI)
2. Bookkeepers
3. Users of the charging service

There are 3 endpoints:
1. `POST /billing/tariff` which creates new tariffs in the system (**requires authentication**)
2. `POST /billing/session` which creates new charge sessions for the users
3. `GET /user/<<userId>>/sessions` return the sessions of the user with the specific `userId`

**In the helpers folder you can find a postman json file. You can import it to postman and you can see sample requests for the application**



## Usage

### Prerequisites
* Java 8 [How to install Oracle JDK 8](https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html)
* Scala plugin for Intellij
* If you are not using intellij, you will also need:
    * sbt [How to install sbt](https://www.scala-sbt.org/release/docs/Setup.html)
    * scala [How to install scala](https://www.scala-lang.org/download/)

### How to run
* If you are working on intellij, just head to the server file and click on the run button
* If you are not using intellij (or prefer the terminal), go to the project folder and type `sbt clean package run`

Both actions will start the webserver at port 8080.

### Testing
You can run the tests with `sbt test`

## Assumptions/Decisions
1. All amounts are in EUR, all amounts are BigDecimals
2. We use RoundingMode.HALF_UP
3. If the user enters a tariff for a date that already exists we should reject
4. A fee cannot be negative(service fee, base fee, parking fee)
5. We cannot stop a tariff. If no other tariff is entered after it then it goes on forever
6. Fees do not change that often (maybe once a day), so we do not have much data to store
7. If a user is not in the system, or has no charge sessions, we return empty in the GET /user/id/sessions response
8. All data are stored in memory
9. If two charge sessions overlap then we reject the second one
10. When a charge session is posted, we return it along with the applicable tariff
11. The prices are shown as numbers with 2 decimal points

## Future improvements

- [ ] Use Joda Money instead of BigDecimal so that we have support for multiple currencies
- [ ] Add currency to the csv data of the GET request
- [ ] Turn model messages into functions to get more details
- [ ] Create real database
- [ ] Split to 3 actors (1 actor per use case)
- [ ] Add [swagger support](https://github.com/swagger-akka-http/swagger-akka-http)
- [ ] Find a better way to store tariffs (maybe sorted set? a map per month? How many times do we change the fees anyway?)
- [ ] Enhance authentication
- [ ] Create config file and add timeouts, passwords etc
- [ ] Split actor into billing and user
- [ ] Refactor Domain errors
